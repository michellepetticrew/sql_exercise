--Show all information about the bond with the CUSIP '28717RH95'.
select * from bond 
	where CUSIP = '28717RH95';

--Show all information about all bonds, in order from shortest (earliest maturity) to longest (latest maturity).
select * from bond
	order by maturity;

--Calculate the value of this bond portfolio, as the sum of the product of each bond's quantity and price.
select sum(quantity * price) value
	from bond

--Show the annual return for each bond, as the product of the quantity and the coupon. 
--Note that the coupon rates are quoted as whole percentage points, so to use them here you will divide their values by 100.
select quantity * coupon/100
	from bond

--Show bonds only of a certain quality and above, for example those bonds with ratings at least AA2. 
--(Don't resort to regular-expression or other string-matching tricks; use the bond-rating ordinal.)
select *  
	from bond
	where rating = 'AA2' or rating = 'AA1' or rating = 'AAA' --rating AA2 or above

--Show the average price and coupon rate for all bonds of each bond rating.
select avg(price) average_price, avg(coupon) average_coupon, rating
	from bond
	group by rating

--Calculate the yield for each bond, as the ratio of coupon to price. Then, identify bonds that we might 
--consider to be overpriced, as those whose yield is less than the expected yield given the rating of the bond.
select b.ID, b.CUSIP, (coupon/price) yield, r.expected_yield
	from bond b
	join rating r on r.rating = b.rating
	where (coupon/price) < r.expected_yield